# memeforth

this has gone too far

this is a FORTH variant that can be used to describe text-based memes

for instance

```forth
'yeet' DNS SUREHOPE HARDWORK
```

results in the text:

```
Just another hard day's work at the 'DNS over yeet? I sure hope it does!' factory
```

being at the top of the stack

more to come later, currently under active development

## maintainers

this project is maintained by [@mdszy](https://mastodon.technology/@mdszy) 
(initial idea and implementation) and 
[@bclindner](https://mastodon.technology/@bclindner) (further improvements and 
integration with Mastodon)