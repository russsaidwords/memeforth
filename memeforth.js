"use strict";

function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand("copy");
    var msg = successful ? "successful" : "unsuccessful";
    console.log("Fallback: Copying text command was " + msg);
  } catch (err) {
    console.error("Fallback: Oops, unable to copy", err);
  }

  document.body.removeChild(textArea);
}
function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(
    function() {
      console.log("Async: Copying to clipboard was successful!");
    },
    function(err) {
      console.error("Async: Could not copy text: ", err);
    }
  );
}

function tokenize(program) {
  var token = "";

  var tokens = [];

  var litstring = false;

  for(var i = 0; i < program.length; i++) {
    var char = program[i]

    if(char.trim() === "" && token.length > 0 && !litstring) {
      tokens.push(token.trim());
      token = ""
    } else if(char === "'" && !litstring) {
      litstring = true;
    } else if(char === "'" && litstring) {
      litstring = false;
      tokens.push(token)
      token = ""
    } else {
      token += char;
    }
  }

  if(token.length > 0)
    tokens.push(token.trim())

  return tokens;
}

function interpret(tokens, startStack) {

  if (startStack) {
    var stack = startStack;
  } else {
    stack = [];
  }

  var words = {
    "HARDWORK": hardwork,
    "SUREHOPE": surehope,
    "HACKER": hackertext,
    "SM64": sm64,
    "DNS": dns,
    "CONCAT": concat,
    "GOTEM": gotem,
    "JORTS": jorts
  }

  var compile = false;

  var currentWord = "";

  for(var i = 0; i < tokens.length; i++) {
    var token = tokens[i];
    
    if(compile) {
      if (token.trim() === ";") compile = false;
      else words[currentWord].push(token);
      continue;
    }

    if(token.trim() === ":") {
      compile = true;
      currentWord = tokens[i+=1]
      words[currentWord] = []
    } else {
      if(words[token])
        if(typeof(words[token]) === "function") {
          words[token](stack);
        } else {
          stack = interpret(words[token], stack)
        }
      else
        stack.push(token);
    }
  }

  return stack;
}

function gotem(stack) {
  stack.push("lmao gottem");

  return stack;
}

/**
 * @summary Converts the top of the stack to a "jorts" meme.
 * @description let me show you how we do JS in the new age old man
 * @param {Array<string>} stack The Forth stack.
 */
const jorts = stack => {
  const subject = stack.pop()
  const jubject = 'j' + subject.substring(1)
  stack.push(`${jubject} (jean ${subject})`)
}

function hardwork(stack) {
  var message = stack.pop()
  var new_str = "Just another hard day's work at the '" + message + "' factory"
  stack.push(new_str);
  return stack;
}

function surehope(stack) {
  var message = stack.pop();
  var new_str = message + "? I sure hope it does!";
  stack.push(new_str);
  return stack
}

function hackertext(stack) {
  var message = stack.pop();
  var new_str = ""

  for(var i = 0; i < message.length; i++) {
    var char = message[i];
    if(/[A-Za-z]/.test(char))
      new_str += ":hacker_" + char.toLowerCase() + ": "
    else
      new_str += char
  }

  stack.push(new_str.trim())

  return stack;
}

function sm64(stack) {
  var message = stack.pop();
  var new_str = ""

  for(var i = 0; i < message.length; i++) {
    var char = message[i];
    if(/[A-Za-z0-9]/.test(char))
      new_str += ":sm64_" + char.toLowerCase() + ": ";
    else if(char === "'")
      new_str += ":sm64_quote: ";
    else if(char === '"')
      new_str += ":sm64_dblquote: ";
    else
      new_str += char;
  }

  stack.push(new_str.trim());

  return stack;
}

function dns(stack) {
  var message = stack.pop();

  var new_str = "DNS over " + message;

  stack.push(new_str);

  return stack;
}

function concat(stack) {
  var b = stack.pop()
  var a = stack.pop()

  stack.push(a + b);

  return stack;
}

$(document).ready(function() {
  $("#runButton").on("click", function(e) {
    var tokens = tokenize($("#inputBox").val())
    var result = interpret(tokens)

    var listStr = "[ ";

    for(var i = 0; i < result.length; i++) {
      listStr += "'" + result[i] + "'"
      if(i < result.length - 1) {
        listStr += ", "
      }
    }

    listStr += " ]"
    $("#stackOutputBox").val(listStr);
    $("#outputBox").val(result[0]);
  });

  $("#copyButton").on("click", function(e) {
    copyTextToClipboard($("#outputBox").val());
  });
});
